(defproject franz "0.1.5-SNAPSHOT"
  :description "A persistent log for clojurescript."
  :license {:name "MIT"}
  :url "https://gitlab.com/j-pb/franz"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.170"]
                 [org.clojure/data.avl "0.0.13"]
                 [tailrecursion/cljs-priority-map "1.1.0"]
                 [org.clojure/test.check "0.9.0"]]
  :plugins [[lein-cljsbuild "1.1.1"]
            [lein-doo "0.1.6"]]
  :cljsbuild
  {:builds [{:id "repl"
             :source-paths ["src"]
             :compiler {:output-to "out/repl.js"
                        :main franz.core
                        :optimizations :none}}
            {:id "test"
             :source-paths ["src" "test"]
             :compiler {:output-to "out/testable.js"
                        :main franz.test.runner
                        :optimizations :none}}]}
  :source-paths ["src" "target/classes"]
  :clean-targets ["out" "release"]
  :target-path "target")
