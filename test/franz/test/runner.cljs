(ns franz.test.runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [franz.test.integration]))

(doo-tests 'franz.test.integration)
