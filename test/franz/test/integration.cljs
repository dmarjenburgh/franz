(ns franz.test.integration
  (:require [cljs.test :as t]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop :include-macros true]
            [clojure.test.check.clojure-test :refer-macros [defspec]]
            [franz.core :as f]))

(defspec new-log-equality
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/any]])
                                       gen/any))]
                       (= (f/log v)
                          (f/log v))))

(defspec new-log-equality-mass
         1000
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/int]])
                                       gen/int))]
                       (= (f/log v)
                          (f/log v))))

(defspec log-indempotent
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/any]])
                                       gen/any))]
                       (= (f/log v)
                          (f/log (f/log v)))))

(defspec log-indempotent-mass
         1000
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/int]])
                                       gen/int))]
                       (= (f/log v)
                          (f/log (f/log v)))))

(defspec log-seq-duality
         10
         (prop/for-all [l (gen/fmap f/log
                                    (gen/vector
                                      (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                                 [1 gen/any]])
                                                 gen/any)))]
                       (= l
                          (f/log (seq l)))))

(defspec log-seq-duality-mass
         1000
         (prop/for-all [l (gen/fmap f/log
                                    (gen/vector
                                      (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                                 [1 gen/int]])
                                                 gen/int)))]
                       (= (f/log l)
                          (f/log (seq l)))))

(defspec log-from-offsets-equality
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple
                              gen/int
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/any]])
                                         gen/any)))
                        n gen/int]
                       (= (f/log-from-offsets v n)
                          (f/log-from-offsets v n))))

(defspec log-from-offsets-equality-mass
         1000
         (prop/for-all [v (gen/vector
                            (gen/tuple
                              gen/int
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/int]])
                                         gen/int)))
                        n gen/int]
                       (= (f/log-from-offsets v n)
                          (f/log-from-offsets v n))))

(defspec log-from-offsets-read-all-duality
         10
         (prop/for-all [l (gen/fmap
                            f/log
                            (gen/vector
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/any]])
                                         gen/any)))]
                       (= l
                          (f/log-from-offsets (f/read-all l)
                                              (count l)))))

(defspec log-from-offsets-read-all-duality-mass
         1000
         (prop/for-all [l (gen/fmap
                            f/log
                            (gen/vector
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/int]])
                                         gen/int)))]
                       (= l
                          (f/log-from-offsets (f/read-all l)
                                              (count l)))))

(defspec conj-grows-log-by-one
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/int]])
                                       gen/int))
                        e (gen/one-of [(gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                                  [1 gen/int]])
                                                  gen/int)
                                       (gen/return nil)])]
                       (= (count (conj (f/log v) e))
                          (inc (count (f/log v))))))

(defspec into-grows-log
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/int]])
                                       gen/int))
                        e (gen/such-that
                            seq
                            (gen/vector
                             (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                        [1 gen/int]])
                                        gen/int)))]
                       (> (count (into (f/log v) e))
                          (count (f/log v)))))

(defspec conj-then-nth
         10
         (prop/for-all [l (gen/fmap
                            f/log
                            (gen/vector
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/any]])
                                         gen/any)))
                        e (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                     [1 gen/any]])
                                     gen/any)]
                       (= (nth (conj l e) (count l))
                          e)))

(defspec conj-then-read
         10
         (prop/for-all [l (gen/fmap
                            f/log
                            (gen/vector
                              (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                         [1 gen/any]])
                                         gen/any)))
                        e (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                     [1 gen/any]])
                                     gen/any)]
                       (= (f/read (conj l e) (count l))
                          [(count l) e])))

(defspec clear-to-offset
         10
         (prop/for-all [v (gen/vector
                            (gen/tuple (gen/frequency [[9 (gen/return nil)]
                                                       [1 gen/any]])
                                       gen/any))
                        n gen/int]
                       (= (seq (f/clear-to-offset (f/log v) n))
                          (seq (concat (repeat (min n (count v)) nil) (drop n v))))))
