(ns franz.implementation
  (:require
    [franz.protocol :as p]
    [clojure.data.avl :as avl]
    [goog.async.nextTick]))

(deftype Log [_meta log key-index write-point]
  p/ILog
  (-read [this offset]
    (avl/nearest log >= offset))

  (-read-all [this offset]
    (-> offset
        dec
        (avl/split-key log)
        (nth 2)
        (seq)))

  (-clear-to-offset [this offset]                            ;TODO maybe check if we actually resize first? Might save some redundant operations here.
    (Log.
      _meta
      (-> offset
          dec
          (avl/split-key log)
          (nth 2))
      (into (empty key-index)
            (filter (fn [[k v]]
                      (< v offset)))
            key-index)
      write-point))

  (-clear-to-size [this size]
    (let [pos (- (count log) size)]
      (if (pos? pos)
        (let [log* (second (avl/split-at pos log))
              retention-point* (nth log* 0)
              key-index* (into (empty key-index)
                               (remove (fn [[k v]] (< v retention-point*)))
                               key-index)]
          (Log.
            _meta
            log*
            key-index*
            write-point))
        this)))

  ICollection
  (-conj [this [k v :as e]]
    (if e
      (Log.
        _meta
        (cond-> log
                (and key (key-index k)) (dissoc (key-index k))
                :do (assoc write-point [k v]))
        (if k
          (assoc key-index k write-point)
          key-index)
        (inc write-point))
      (Log.
        _meta
        log
        key-index
        (inc write-point))))

  IEmptyableCollection
  (-empty [this]
    (Log. nil
          (empty log)
          (empty key-index)
          0))

  IIndexed
  (-nth [this n]
    (get log n))
  (-nth [this n not-found]
    (get log n not-found))

  ISeqable
  (-seq [this]
    (seq (map log (range write-point))))

  ICounted
  (-count [this]
    write-point)

  Object
  (toString [this]
    (str (seq this)))

  IPrintWithWriter
  (-pr-writer [this writer opts]
    (pr-sequential-writer writer pr-writer "(" " " ")" opts this))

  IEquiv                                                    ;TODO See if we want this behavoir? (= [] () (log))
  (-equiv [this that]
    (and (= log (.-log that))
         (= write-point (.-write-point that))))

  IMeta
  (-meta [this]
    _meta)

  IWithMeta
  (-with-meta [this meta]
    (Log. meta log key-index write-point)))

(deftype Topic [^:mutable log ^:mutable handlers ^:mutable handler-positions ^:mutable handler-auto-schedules ^:mutable idle max-size]
  p/ITopic
  (-send!
    [this msg]
    (p/-send! this nil msg))
  (-send!
    [this key msg]
    (set! log (conj log [key msg]))
    (p/-compact! this)
    (p/-schedule! this)
    this)
  (-subscribe!
    [this key position auto-schedule handler]
    (set! handlers (assoc handlers key handler))
    (set! handler-auto-schedules (assoc handler-auto-schedules key auto-schedule))
    (set! handler-positions (assoc handler-positions key position))
    (p/-compact! this)
    (when auto-schedule
      (p/-schedule! this))
    this)
  (-unsubscribe! [this key]
    (set! handlers (dissoc handlers key))
    (set! handler-auto-schedules (dissoc handler-auto-schedules key))
    (set! handler-positions (dissoc handler-positions key))
    this)
  (-tick! [this]
    (let [handler-key (first (filter handler-auto-schedules (keys handler-positions)))]
      (p/-tick! this handler-key false)))
  (-tick! [this key force]
    (let [position (handler-positions key)
          handler (handlers key)]
      (when (and position (or force (< position (count log))))
        (let [new-position (handler log position)]
          (set! handler-positions (assoc handler-positions
                                    key
                                    new-position))
          [key new-position]))))
  (-flush! [this]
    (while (p/-tick! this))
    (p/-compact! this))
  (-flush! [this key]
    (while (p/-tick! this key false))
    (p/-compact! this))
  (-positions [this]
    handler-positions)
  (-set-positions! [this positions]
    (set! handler-positions (merge handler-positions positions))
    this)
  (-schedule! [this]
    (if idle
      (do
        (set! idle false)
        (goog.async.nextTick
          (fn tick! []
            (if (p/-tick! this)
              (do
                (set! idle false)
                (goog.async.nextTick tick!))
              (set! idle true))
            (p/-compact! this)))
        true)
      false))
  (-compact! [this]
    (when max-size
      (let [[handler-key position] (first handler-positions)]
        (when (< max-size (count log))
          (if (< max-size (- (count log) position))
            (set! log (p/-clear-to-offset log position))
            (set! log (p/-clear-to-size log max-size))))))
    this)
  IDeref
  (-deref [this]
    log)
  IReset
  (-reset! [this new-log]
    (set! log new-log)
    (set! handler-positions (into (empty handler-positions)
                                  (map vector
                                       (keys handler-positions)
                                       (repeat 0))))
    (p/-schedule! this)
    this))
