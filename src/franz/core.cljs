(ns franz.core
  (:require
    [franz.protocol :as p]
    [franz.implementation :as imp]
    [clojure.data.avl :as avl]
    [tailrecursion.priority-map :as pm]))

;;Log;;
(defn log
  "Creates a persistent log from the given key value pairs."
  ([col]
   (into (log)
         col))
  ([]
   (imp/->Log nil (avl/sorted-map-by <) {} 0)))

(defn log-from-offsets
  "Creates a log from the given offset entry pairs.
  When a size is provided the actual size will be the maximum of the size and the largest offset."
  ([offsets] (log-from-offsets offsets 0))
  ([offsets size]
   (let [key-offsets (apply merge-with max {}
                            (map (fn [[o [k _]]]
                                   (when k {k o}))
                                 offsets))
         cleaned-offsets (filter (fn [[o [k _]]]
                                   (if k
                                     (= o (key-offsets k))
                                     true))
                                 offsets)
         max-offset (apply max (map first offsets))
         max-size (max size
                       (if max-offset
                         (inc max-offset)
                         0))]
     (imp/->Log nil
                (into (avl/sorted-map-by <) cleaned-offsets)
                key-offsets
                max-size))))

(defn read
  "Reads the next message at or after the given offset.
  Returns a pair of the next offset and it's message.
  Or nil when no such message exists."
  [log offset]
  (p/-read log offset))

(defn read-all
  "Reads all messages starting at or after the given offset.
  Returns a seq of pairs of the next offset and it's message."
  ([log]
   (p/-read-all log 0))
  ([log offset]
   (p/-read-all log offset)))

(defn clear-to-offset
  "Moves the retention point to the given offset,
  this drops all messages up to it, excluding."
  [log offset]
  (p/-clear-to-offset log offset))

(defn clear-to-size
  "Moves the retention point by dropping messages until the log has the given size."
  [log size]
  (p/-clear-to-size log size))


;;Topic;;
(defn topic
  "Creates a new topic that holds a log and manages asynchronous execution of subscribed handling functions.
  A custom posibly prefilled log can be provided. When ommited or nil an empty franz.core/log will be created.
  Optionally opts with default {:size nil} can be passed.
  When a size is provided, it will be used as a soft limit to shrink the log to, while nil means no shrinking is done.
  Old messages will only be disgarded however when every registered handler has read them."
  ([]
   (topic nil nil))
  ([log]
    (topic log nil))
  ([log {:keys [size]
         :or {size nil}}]
   (imp/->Topic
     (or log (franz.core/log))
     {}
     (pm/priority-map-by <)
     {}
     true
     size)))


(defn send!
  "Writes the given message onto the internal log.
  If a key is provided the previously existing message under this key will be replaced."
  ([topic msg]
   (p/-send! topic msg))
  ([topic key msg]
   (p/-send! topic key msg)))

(defn subscribe!
  "Adds the given handler to the list of subscribers under the given key.
  The subscribing handler function will be called with the log and it's currend offset position,
  and must return the new offset position.
  Optionally opts with default {:position 0, :auto-schedule true} can be passed.
  The optional size is the offset at which the handler will start consuming from the log, and defaults to 0.
  When auto-schedule is set to false sheduling has to be started explicitly with the schedule! function."
  ([topic key handler]
    (subscribe! topic key handler {}))
  ([topic key handler {:keys [position
                              auto-schedule]
                       :or   {position      0
                              auto-schedule true}
                       :as   opts}]
   (p/-subscribe! topic key position auto-schedule handler)))

(defn unsubscribe!
  "Removes the handler for the given key."
  [topic key]
  (p/-unsubscribe! topic key))

(defn positions
  "Returns a map containing the next processed position/offset for each subscription function."
  [topic]
  (p/-positions topic))

(defn set-positions!
  "Sets the positions of the subscribers to those provided as a mapping from key to offset.
  Omitted subscribers won't be changed."
  [topic positions]
  (p/-set-positions! topic positions))

(defn flush!
  "Executes the subscription function for the given key exhaustively over the given topic
   until they are all positioned at the latest offset.
   If no key is provided all handlers will be flushed."
  ([topic]
   (p/-flush! topic))
  ([topic key]
   (p/-flush! topic key)))

(defn tick!
  "Advanced. This should only be used if you _absolutely need_ fine grained timing control over sheduling behaviour.
  To be used in conjunction with {:auto-schedule false} option on subscribers.
  Executes the subscription function registered under the given key once on the given topic.
  If no key is provided the subscriber most behind the head of the log will be executed.
  Returns the key of the executed subscriber and its new offset,
  and nil if nothing needed to be executed.
  If force is set to true however the handler in question will be executed
  even if it is already at the head."
  ([topic]
   (p/-tick! topic))
  ([topic key]
   (p/-tick! topic key false))
  ([topic key force]
   (p/-tick! topic key force)))
