(ns franz.protocol)

(defprotocol ILog
  (-read [this offset])
  (-read-all [this offset])
  (-clear-to-offset [this offset])
  (-clear-to-size [this size]))

(defprotocol ITopic
  (-send! [this msg] [this key msg])
  (-subscribe! [this key position auto-schedule handler])
  (-unsubscribe! [this key])
  (-positions [this])
  (-set-positions! [this positions])
  (-flush! [this] [this key])
  (-tick! [this] [this key force])
  (-schedule! [this])
  (-compact! [this]))
